import React, { useState, useEffect } from "react";
import { Avatar, Button, Input } from "@material-ui/core";
import "./Post.css";

const BASE_URL = "http://localhost:8000/";

function Post({ post, authToken, authTokenType, username }) {
  console.log(post);
  const [imageURL, setImageURL] = useState("");
  const [comments, setComments] = useState([]);
  const [newComment, setNewComment] = useState("");

  useEffect(() => {
    if (post.image_url_type === "absolute") {
      setImageURL(post.image_url);
    } else {
      setImageURL(BASE_URL + post.image_url);
    }
  }, []);

  useEffect(() => {
    setComments(post.comments);
  }, []);

  const handleDelete = (e) => {
    e?.preventDefault();

    const requestOptions = {
      method: "GET",
      headers: new Headers({
        Authorization: authTokenType + " " + authToken,
        "Content-Type": "application/json",
      }),
    };

    fetch(BASE_URL + "post/delete/" + post.id, requestOptions)
      .then((response) => {
        if (response.ok) {
          window.location.reload();
        }
      })
      .catch((err) => console.log(err));
  };

  const postComment = (e) => {
    e?.preventDefault();
    const json_string = JSON.stringify({
      username: username,
      text: newComment,
      post_id: post.id,
    });

    const requestOptions = {
      method: "POST",
      headers: new Headers({
        Authorization: authTokenType + " " + authToken,
        "Content-Type": "application/json",
      }),
      body: json_string,
    };
    fetch(BASE_URL + "comments/create/", requestOptions)
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
      })
      .then((data) => fetchComments())
      .catch((err) => console.log(err))
      .finally(() => {
        setNewComment("");
      });
  };

  const fetchComments = () => {
    fetch(BASE_URL + "comments/comments/" + post.id)
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
        throw response;
      })
      .then((data) => {
        setComments(data);
      })
      .catch((err) => console.log(err));
  };

  return (
    <div className="post">
      <div className="post_header">
        <Avatar alt="Pedro" src="" />
        <div className="post_headerInfo">
          <h3>{post.user.username}</h3>
          <Button className="post_delete" onClick={handleDelete}>
            Delete
          </Button>
        </div>
      </div>
      <img className="post_image" src={imageURL} alt={post.caption} />

      <h4 className="post_text">{post.caption}</h4>

      <div className="post_comments">
        {comments.map((comment) => (
          <p>
            <strong>{comment.username}: </strong> {comment.text}
          </p>
        ))}
      </div>

      {authToken && (
        <form className="post_commentbox">
          <Input
            type="text"
            placeholder="add comment"
            value={newComment}
            className="post_input"
            onChange={(e) => setNewComment(e.target.value)}
          />
          <Button
            className="post_button"
            type="submit"
            disabled={!newComment}
            onClick={postComment}
          >
            Post
          </Button>
        </form>
      )}
    </div>
  );
}

export default Post;
