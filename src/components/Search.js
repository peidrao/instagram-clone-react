import { useState, useEffect } from "react";
import { Button, Modal, makeStyles, Input } from "@material-ui/core";
import api from "../services/api";

function Search({ user_id }) {
  const [username, setUsername] = useState("");

  const searchUser = (event) => {
    event?.preventDefault();
    api
      .get(`user/${username}`)
      .then((response) => console.log(response))
      .catch((error) => console.log(error));
  };

  return (
    <div>
      <Input
        text="text"
        placeholder="Search by user"
        onChange={(e) => setUsername(e.target.value)}
      />
      <Button type="submit" onClick={searchUser}>
        SEARCH
      </Button>
    </div>
  );
}

export default Search;
