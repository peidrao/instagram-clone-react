import React, { useState } from "react";
import { Button, Input } from "@material-ui/core";

import "./ImageUpload.css";

const BASE_URL = "http://localhost:8000/";

function ImageUpload({ authToken, authTokenType, userId }) {
  const [caption, setCaption] = useState("");
  const [image, setImage] = useState(null);

  const handleChange = (e) => {
    if (e.target.files[0]) {
      setImage(e.target.files[0]);
    }
    console.log(e.target.files[0]);
  };

  const handleUpload = (e) => {
    e?.preventDefault();

    const formData = new FormData();
    formData.append("image", image);

    const requestOptions = {
      method: "POST",
      headers: new Headers({ Authorization: authTokenType + " " + authToken }),
      body: formData,
    };

    fetch(BASE_URL + "post/upload/", requestOptions)
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
        throw response;
      })
      .then((data) => createPost(data.filename))
      .catch((error) => console.log(error))
      .finally(() => {
        setCaption("");
        setImage(null);
        document.getElementById("fileInput").value = null;
      });
  };

  const createPost = (imageUrl) => {
    const json_string = JSON.stringify({
      image_url: imageUrl,
      image_url_type: "relative",
      caption: caption,
      user_id: userId,
    });

    const requestOptions = {
      method: "POST",
      headers: new Headers({
        Authorization: authTokenType + " " + authToken,
        "Content-Type": "application/json",
      }),
      body: json_string,
    };

    fetch(BASE_URL + "post/create/", requestOptions)
      .then((response) => {
        if (response.ok) {
          return response.json();
        }
        throw response;
      })
      .then((data) => {
        window.location.reload();
        window.scrollTo(0, 0);
      })
      .catch((err) => console.log(err));
  };

  return (
    <div className="imageupload">
      <Input
        type="text"
        placeholder="Enter a caption"
        onChange={(e) => setCaption(e.target.value)}
        value={caption}
      />

      <Input type="file" id="fileInput" onChange={handleChange} />

      <Button className="imageupload_button" onClick={handleUpload}>
        Upload
      </Button>
    </div>
  );
}

export default ImageUpload;
